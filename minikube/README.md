# Menggunakan Minikube #

0. Memastikan instalasi minikube

    ```
    minikube version
    ```

    Outputnya

    ```
    minikube version: v1.24.0
    commit: 76b94fb3c4e8ac5062daf70d60cf03ddcc0a741b
    ```

1. Tidak perlu membuat kubernetes cluster, langsung start saja

    ```
    minikube start
    ```

    Apabila terjadi error `No space left on device` seperti ini 

    ```
    ❌  Exiting due to K8S_INSTALL_FAILED: updating control plane: downloading binaries: copybinary /Users/endymuhardin/.minikube/cache/linux/v1.22.3/kubeadm -> /var/lib/minikube/binaries/v1.22.3/kubeadm: copy: sudo test -d /var/lib/minikube/binaries/v1.22.3 && sudo scp -t /var/lib/minikube/binaries/v1.22.3 && sudo touch -d "2021-11-10 09:29:11.429948619 +0700" /var/lib/minikube/binaries/v1.22.3/kubeadm: Process exited with status 1
    output: ��scp: /var/lib/minikube/binaries/v1.22.3/kubeadm: No space left on device
    ```

    Atau seperti ini

    ```
    🧯  Docker is nearly out of disk space, which may cause deployments to fail! (98% of capacity)
    💡  Suggestion: 

        Try one or more of the following to free up space on the device:
        
        1. Run "docker system prune" to remove unused Docker data (optionally with "-a")
        2. Increase the storage allocated to Docker for Desktop by clicking on:
        Docker icon > Preferences > Resources > Disk Image Size
        3. Run "minikube ssh -- docker system prune" if using the Docker container runtime
    🍿  Related issue: https://github.com/kubernetes/minikube/issues/9024
    ```

    Kita perlu menambah ukuran disk dengan perintah berikut

    ```
    minikube config set disk-size 24000 
    ```

    Output

    ```
    ❗  These changes will take effect upon a minikube delete and then a minikube start
    ```

    Delete dulu minikube cluster

    ```
    minikube delete
    ```

    Outputnya

    ```
    🔥  Deleting "minikube" in docker ...
    🔥  Deleting container "minikube" ...
    🔥  Removing /Users/endymuhardin/.minikube/machines/minikube ...
    💀  Removed all traces of the "minikube" cluster.
    ```

    Kemudian coba lagi `minikube start`

    Outputnya seperti ini 

    ```
    🔥  Deleting "minikube" in docker ...
    🔥  Deleting container "minikube" ...
    🔥  Removing /Users/endymuhardin/.minikube/machines/minikube ...
    💀  Removed all traces of the "minikube" cluster.
    endymuhardin@Endys-MacBook-Pro belajar-kubernetes % minikube start                     
    😄  minikube v1.24.0 on Darwin 12.0.1 (arm64)
    ✨  Automatically selected the docker driver
    👍  Starting control plane node minikube in cluster minikube
    🚜  Pulling base image ...
    🔥  Creating docker container (CPUs=2, Memory=1988MB) ...
    🐳  Preparing Kubernetes v1.22.3 on Docker 20.10.8 ...
        ▪ Generating certificates and keys ...
        ▪ Booting up control plane ...
        ▪ Configuring RBAC rules ...
    🔎  Verifying Kubernetes components...
        ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
    🌟  Enabled addons: storage-provisioner, default-storageclass
    🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
    ```

2. Melihat informasi tentang cluster

    ```
    kubectl cluster-info
    ```

    Output

    ```
    Kubernetes control plane is running at https://127.0.0.1:50298
    CoreDNS is running at https://127.0.0.1:50298/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

    To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
    ```

3. Melihat informasi tentang node

    ```
    kubectl get nodes
    ```

    Outputnya

    ```
    NAME       STATUS   ROLES                  AGE   VERSION
    minikube   Ready    control-plane,master   72s   v1.22.3
    ```

4. Mempersiapkan konfigurasi deployment untuk aplikasi belajar-docker

    * [persistent volume](01-persistent-volume.yml)
    * [database postgres](02-database-postgres.yml)
    * [database service](03-database-service.yml)
    * [aplikasi belajar-docker](04-aplikasi-web.yml)
    * [load balancer supaya aplikasi web bisa diakses](05-aplikasi-web-service.yml)

5. Mendeploy konfigurasi

    ```
    kubectl -f namafile.yml apply
    ```

6. Melihat daftar object yang dibuat pada waktu deployment

    ```
    kubectl get pv, pvc, 
    kubectl get pods
    kubectl get deployments 
    kubectl get services
    ```

7. Melihat log container

    ```
    kubectl logs nama-pod
    ```

8. Mempublish service `NodePort` di minikube

    ```
    minikube service --url nama-service
    ```

    Outputnya seperti ini

    ```
    🏃  Starting tunnel for service db-belajar-nodeport.
    |-----------|---------------------|-------------|------------------------|
    | NAMESPACE |        NAME         | TARGET PORT |          URL           |
    |-----------|---------------------|-------------|------------------------|
    | default   | db-belajar-nodeport |             | http://127.0.0.1:53999 |
    |-----------|---------------------|-------------|------------------------|
    http://127.0.0.1:53999
    ❗  Because you are using a Docker driver on darwin, the terminal needs to be open to run it.
    ```

    Selanjutnya kita bisa connect ke database dengan IP dan Port tersebut

    ```
    psql -h 127.0.0.1 -p 53999 -U belajar -d belajar-docker
    ```

9. Menghapus objek (pv, pvc, pod, deployment, service)

    ```
    kubectl delete nama-object
    ```

10. Melakukan replikasi (menambah atau mengurangi instance / jumlah pod)

    Lihat dulu nama deployment yang akan di-scale

    ```
    kubectl get deployments
    ```

    Outputnya

    ```
    NAME              READY   UP-TO-DATE   AVAILABLE   AGE
    db-belajar-dev    1/1     1            1           176m
    web-belajar-dev   1/1     1            1           5m41s
    ```

    Scale deployment bernama `web-belajar-dev`

    ```
    kubectl scale --replicas=3 deployment/web-belajar-dev
    ```

    Cek lagi jumlah pods

    ```
    kubectl get pods
    ```

    Outputnya seperti ini

    ```
    NAME                               READY   STATUS              RESTARTS      AGE
    db-belajar-dev-67b9fd46bc-zmzqx    1/1     Running             1 (51m ago)   176m
    web-belajar-dev-844b6b6d6c-2hvpr   0/1     Pending             0             2s
    web-belajar-dev-844b6b6d6c-hjd59   0/1     ContainerCreating   0             2s
    web-belajar-dev-844b6b6d6c-vss7t   1/1     Running             0             6m
    ```

    Cek status deployment

    ```
    kubectl get deployments
    ```

    Outputnya

    ```
    NAME              READY   UP-TO-DATE   AVAILABLE   AGE
    db-belajar-dev    1/1     1            1           176m
    web-belajar-dev   1/3     1            1           5m41s
    ```

11. Menghapus objek yang dideploy dengan file konfigurasi tertentu

    ```
    kubectl delete -f 05-aplikasi-web-service-lb.yml
    ```

12. Menjalankan service bertipe LoadBalancer di minikube

    ```
    kubectl apply -f 05-aplikasi-web-service-lb.yml
    ```

    Cek status service

    ```
    kubectl get services
    ```

    Cek status `External IP`, masih pending. Artinya belum mendapatkan alamat IP yang bisa diakses dari luar cluster

    ```
    NAME                  TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
    app-belajar-service   LoadBalancer   10.98.224.51     <pending>     8080:30224/TCP   2s
    ```

13. Jalankan minikube tunnel, agar service LoadBalancer bisa berfungsi dengan baik

    ```
    minikube tunnel
    ```

    Outputnya seperti ini 

    ```
    🏃  Starting tunnel for service app-belajar-service.
    ```

    Cek lagi status `External-IP` dengan perintah `kubectl get services`. Harusnya sekarang sudah mendapatkan alamat IP

    ```
    NAME                  TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
    app-belajar-service   LoadBalancer   10.98.224.51     127.0.0.1     8080:30224/TCP   102s
    ```

    Aplikasi bisa diakses di `http://127.0.0.1:8080`

14. Mengupdate docker image dengan versi yang lebih baru

    Tampilkan nama deployment

    ```
    kubectl get deployments
    ```

    Edit konfigurasi deployment

    ```
    kubectl edit deployment/web-belajar-dev
    ```

    Misalnya ganti `image: endymuhardin/belajar-docker` menjadi `image: endymuhardin/belajar-docker:202111101500`

    Kemudian save. Kubernetes akan mengupdate deployment.

    ```
    Waiting for deployment "web-belajar-dev" rollout to finish: 1 out of 2 new replicas have been updated...
    Waiting for deployment "web-belajar-dev" rollout to finish: 1 out of 2 new replicas have been updated...
    Waiting for deployment "web-belajar-dev" rollout to finish: 1 out of 2 new replicas have been updated...
    Waiting for deployment "web-belajar-dev" rollout to finish: 1 old replicas are pending termination...
    Waiting for deployment "web-belajar-dev" rollout to finish: 1 old replicas are pending termination...
    deployment "web-belajar-dev" successfully rolled out
    ```

    Kita bisa browse/test aplikasi untuk memastikan versi baru telah terdeploy.

# Referensi #

* [Tutorial Resmi Minikube](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-interactive/)
* [Membuat Persistent Volume di Minikube](https://minikube.sigs.k8s.io/docs/handbook/accessing/)
* [Konfigurasi Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
* [Menggunakan Environment Variable](https://kubernetes.io/docs/tasks/inject-data-application/define-environment-variable-container/)
* [Mendeploy PostgreSQL di Kubernetes](https://www.sumologic.com/blog/kubernetes-deploy-postgres/)
* [Mengakses Service dengan NodePort](https://minikube.sigs.k8s.io/docs/handbook/accessing/)
* [Daftar Perintah kubectl](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
* [Kubernetes Update Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
* [Strategi Deployment dan Update](https://auth0.com/blog/deployment-strategies-in-kubernetes/)