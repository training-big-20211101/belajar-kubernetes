# Membuat Kubernetes Cluster di Digital Ocean #

1. Buat VPS dengan RAM 4GB

2. SSH ke VPS

3. Update dan upgrade package

    ```
    apt update && apt upgrade -y && reboot
    ```

4. Jalankan script instalasi k3s

    ```
    curl -sfL https://get.k3s.io | sh -
    ```

    Outputnya seperti ini 

    ```
    [INFO]  Finding release for channel stable
    [INFO]  Using v1.21.5+k3s2 as release
    [INFO]  Downloading hash https://github.com/k3s-io/k3s/releases/download/v1.21.5+k3s2/sha256sum-amd64.txt
    [INFO]  Downloading binary https://github.com/k3s-io/k3s/releases/download/v1.21.5+k3s2/k3s
    [INFO]  Verifying binary download
    [INFO]  Installing k3s to /usr/local/bin/k3s
    [INFO]  Skipping installation of SELinux RPM
    [INFO]  Creating /usr/local/bin/kubectl symlink to k3s
    [INFO]  Creating /usr/local/bin/crictl symlink to k3s
    [INFO]  Creating /usr/local/bin/ctr symlink to k3s
    [INFO]  Creating killall script /usr/local/bin/k3s-killall.sh
    [INFO]  Creating uninstall script /usr/local/bin/k3s-uninstall.sh
    [INFO]  env: Creating environment file /etc/systemd/system/k3s.service.env
    [INFO]  systemd: Creating service file /etc/systemd/system/k3s.service
    [INFO]  systemd: Enabling k3s unit
    Created symlink /etc/systemd/system/multi-user.target.wants/k3s.service → /etc/systemd/system/k3s.service.
    [INFO]  systemd: Starting k3s
    ```

5. Mengakses informasi cluster dari laptop

    * Copy file `/etc/rancher/k3s/k3s.yaml` ke laptop
    * Edit file tersebut, ganti alamat IP localhost dengan alamat server. Contohnya seperti ini, `127.0.0.1` diganti menjadi `159.223.60.192`

        ```yml
        apiVersion: v1
        clusters:
        - cluster:
            certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJlRENDQVIyZ0F3SUJBZ0lCQURBS0JnZ3Foa2pPUFFRREFqQWpNU0V3SHdZRFZRUUREQmhyTTNNdGMyVnkKZG1WeUxXTmhRREUyTXpZMU9UZ3dOREV3SGhjTk1qRXhNVEV4TURJek5EQXhXaGNOTXpFeE1UQTVNREl6TkRBeApXakFqTVNFd0h3WURWUVFEREJock0zTXRjMlZ5ZG1WeUxXTmhRREUyTXpZMU9UZ3dOREV3V1RBVEJnY3Foa2pPClBRSUJCZ2dxaGtqT1BRTUJCd05DQUFRZWlhdDFkcnlkMTd3d1o1UU5BdFlEUEJEclVuYTl5N2FuWFlqazQyc0UKY2dGOHY1Wkd5QTJyU0EvZGlzc0xFZTErT1hwM3lLY210UDdpNmFBblV5TDBvMEl3UURBT0JnTlZIUThCQWY4RQpCQU1DQXFRd0R3WURWUjBUQVFIL0JBVXdBd0VCL3pBZEJnTlZIUTRFRmdRVW9nQWl0ekpEQlQwRnJFMUNULzZmCjh2ZVEvcG93Q2dZSUtvWkl6ajBFQXdJRFNRQXdSZ0loQUtjVkF2QWxBUEI3RUp0Ri9rRG1jTkhyMHlyOEtiYUMKQlFtVVVseVRjMlI5QWlFQW5oQ2JXQWlYRzFiQ01oYVgzVVVxMytVTklvWWZleXFnRGVYUDBUbm5JMkE9Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
            server: https://159.223.60.192:6443
        name: default
        contexts:
        - context:
            cluster: default
            user: default
        name: default
        current-context: default
        kind: Config
        preferences: {}
        users:
        - name: default
        user:
            client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJrRENDQVRlZ0F3SUJBZ0lJSTNMNlFiRjlZNkF3Q2dZSUtvWkl6ajBFQXdJd0l6RWhNQjhHQTFVRUF3d1kKYXpOekxXTnNhV1Z1ZEMxallVQXhOak0yTlRrNE1EUXhNQjRYRFRJeE1URXhNVEF5TXpRd01Wb1hEVEl5TVRFeApNVEF5TXpRd01Wb3dNREVYTUJVR0ExVUVDaE1PYzNsemRHVnRPbTFoYzNSbGNuTXhGVEFUQmdOVkJBTVRESE41CmMzUmxiVHBoWkcxcGJqQlpNQk1HQnlxR1NNNDlBZ0VHQ0NxR1NNNDlBd0VIQTBJQUJGNzB3K1Vvc1FOais0a2QKci9FQ2JOV3dyMlhKRHNscDVzeUExTHNpWCtiSXpObDhWU3NpaDRQNGkxMzNMTFlscm1yWkpGQzYrdFh1MVh2Zwp1bE42YnFTalNEQkdNQTRHQTFVZER3RUIvd1FFQXdJRm9EQVRCZ05WSFNVRUREQUtCZ2dyQmdFRkJRY0RBakFmCkJnTlZIU01FR0RBV2dCU1JpLzVDdk5GY1VuLzQ4bmRUUzl6Q1FETWgyVEFLQmdncWhrak9QUVFEQWdOSEFEQkUKQWlBLzc0ZWl5OThjZEJydWFBR1EzSnRpVEhMMWQ4cGNIUVFUb1dVc3k2ZGhvUUlnTFBqclNQOVVCNmtBNFpzdwoxZWpqVDNpWnN6UGRlYnpVOEtJa0l6OHlzTW89Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0KLS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJkekNDQVIyZ0F3SUJBZ0lCQURBS0JnZ3Foa2pPUFFRREFqQWpNU0V3SHdZRFZRUUREQmhyTTNNdFkyeHAKWlc1MExXTmhRREUyTXpZMU9UZ3dOREV3SGhjTk1qRXhNVEV4TURJek5EQXhXaGNOTXpFeE1UQTVNREl6TkRBeApXakFqTVNFd0h3WURWUVFEREJock0zTXRZMnhwWlc1MExXTmhRREUyTXpZMU9UZ3dOREV3V1RBVEJnY3Foa2pPClBRSUJCZ2dxaGtqT1BRTUJCd05DQUFSSHBKbDFqTzc0NjhrU1pNMDl1YVBJNVpsdzBCS1JsVSs1VGR2SXhSaXgKQjVubVRDK3RJOXh4dHNheUYwQWNTTFhZczRwYUJ0ZWdqUW5WenJVR2JJK2hvMEl3UURBT0JnTlZIUThCQWY4RQpCQU1DQXFRd0R3WURWUjBUQVFIL0JBVXdBd0VCL3pBZEJnTlZIUTRFRmdRVWtZditRcnpSWEZKLytQSjNVMHZjCndrQXpJZGt3Q2dZSUtvWkl6ajBFQXdJRFNBQXdSUUlnSVFhVkVZL3ltUmp3SStGMjdmb01VeWFFQTZRL3l3MTgKYzl1QThveTFsTklDSVFEMDlMUmFaZ3FqVms5Z0QzNE1RZmtxeG1NVHg5b1JoOXVqdlJrc0w0UFd0dz09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
            client-key-data: LS0tLS1CRUdJTiBFQyBQUklWQVRFIEtFWS0tLS0tCk1IY0NBUUVFSUozUUtxS0IrTzF4cVBMMElEZlljajFROGNxTGNNTzR0U2R1WlphRmZFTlhvQW9HQ0NxR1NNNDkKQXdFSG9VUURRZ0FFWHZURDVTaXhBMlA3aVIydjhRSnMxYkN2WmNrT3lXbm16SURVdXlKZjVzak0yWHhWS3lLSApnL2lMWGZjc3RpV3VhdGtrVUxyNjFlN1ZlK0M2VTNwdXBBPT0KLS0tLS1FTkQgRUMgUFJJVkFURSBLRVktLS0tLQo=
        ```
    
    * Set environment variable di sistem operasi untuk menunjuk ke file tersebut

        ```
        export KUBECONFIG=/Users/endymuhardin/digitalocean-k3s/kubeconfig.yml
        ```
    
    * Tes akses ke k8s cluster

        ```
        kubectl cluster-info
        ```

        Outputnya seperti ini

        ```
        Kubernetes control plane is running at https://159.223.60.192:6443
        CoreDNS is running at https://159.223.60.192:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
        Metrics-server is running at https://159.223.60.192:6443/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy
        ```

        Dan informasi node

        ```
        kubectl get nodes
        ```

        Outputnya seperti ini

        ```
        NAME         STATUS   ROLES                  AGE    VERSION
        k8s-node-1   Ready    control-plane,master   9m6s   v1.21.5+k3s2
        endymuhardin@Endys-MacBook-Pro digitalocean-k3s % kubectl cluster-info
        ```

6. Menambah node baru ke dalam cluster

    * Ambil `token` di server pertama

        ```
        cat /var/lib/rancher/k3s/server/node-token
        ```

        Hasilnya seperti ini

        ```
        K10348f5f64f64e40ec3deb89d9c54a3aa390f5e170411ef3e57bf2584bf70a6235::server:3311fae99d9d05dbb3c811e451efa76b
        ```

    * Instal OS (Ubuntu), update & upgrade package 
    
        ```
        apt update && apt upgrade && reboot
        ```

    * Instal k3s dengan menyebutkan URL server pertama dan token

        ```
        curl -sfL https://get.k3s.io | K3S_URL=https://159.223.60.192:6443 K3S_TOKEN=K10348f5f64f64e40ec3deb89d9c54a3aa390f5e170411ef3e57bf2584bf70a6235::server:3311fae99d9d05dbb3c811e451efa76b sh -
        ```

    * Set rolenya sebagai `worker`

        ```
        kubectl label node k8s-node-2 node-role.kubernetes.io/worker=worker
        ```


# Mendeploy Aplikasi Belajar Docker ke K8S DO #

1. Mendeploy Persistent Volume

    ```
    kubectl apply -f https://gitlab.com/training-big-20211101/belajar-kubernetes/-/raw/master/minikube/01-persistent-volume.yml
    ```

2. Mendeploy Database PostgreSQL

    ```
    kubectl apply -f https://gitlab.com/training-big-20211101/belajar-kubernetes/-/raw/master/minikube/02-database-postgres.yml
    ```

3. Mendeploy Service Database

    ```
    kubectl apply -f https://gitlab.com/training-big-20211101/belajar-kubernetes/-/raw/master/minikube/03-database-service-clusterip.yml
    ```

4. Mendeploy Aplikasi Web

    ```
    kubectl apply -f https://gitlab.com/training-big-20211101/belajar-kubernetes/-/raw/master/minikube/04-aplikasi-web.yml
    ```

5. Mendeploy Load Balancer

    ```
    kubectl apply -f https://gitlab.com/training-big-20211101/belajar-kubernetes/-/raw/master/minikube/05-aplikasi-web-service-lb.yml
    ```

6. Scale (menambah/mengurangi replica) 

    ```
    kubectl scale --replicas=10 deployment/web-belajar-dev
    ```

7. Melihat jumlah pod dan lokasinya

    ```
    kubectl get pods -o wide
    ```

8. Mengaktifkan Horizontal Pod Autoscaler

    ```
    kubectl autoscale deployment web-belajar-dev --cpu-percent=70 --min=1 --max=10
    ```

9. Mengecek status HPA

    ```
    kubectl get hpa
    ```

    Outputnya seperti ini

    ```
    NAME              REFERENCE                    TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
    web-belajar-dev   Deployment/web-belajar-dev   9%/50%   1         10        10         8m5s
    ```

10. Mengakses aplikasi dengan jumlah request yang besar

    ```
    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://app-belajar-service:8080/waktu; done"
    ```