# Menggunakan K3S #

Untuk memudahkan pengelolaan K3S, kita menggunakan aplikasi `k3d`

0. Memastikan instalasi k3d

    ```
    k3d --version
    ```

    Outputnya

    ```
    k3d version v5.0.3
    k3s version latest (default)
    ```

1. Membuat cluster

    ```
    k3d cluster create belajar-k3s
    ```

    Outputnya

    ```
    INFO[0000] Prep: Network                                
    INFO[0000] Created network 'k3d-belajar-k3s'            
    INFO[0000] Created volume 'k3d-belajar-k3s-images'      
    INFO[0000] Starting new tools node...                   
    INFO[0000] Starting Node 'k3d-belajar-k3s-tools'        
    INFO[0001] Creating node 'k3d-belajar-k3s-server-0'     
    INFO[0001] Creating LoadBalancer 'k3d-belajar-k3s-serverlb' 
    INFO[0001] Using the k3d-tools node to gather environment information 
    WARN[0002] failed to resolve 'host.docker.internal' from inside the k3d-tools node: Failed to read address for 'host.docker.internal' from command output 
    INFO[0002] HostIP: using network gateway...             
    INFO[0002] Starting cluster 'belajar-k3s'               
    INFO[0002] Starting servers...                          
    INFO[0003] Starting Node 'k3d-belajar-k3s-server-0'     
    INFO[0003] Deleted k3d-belajar-k3s-tools                
    INFO[0008] Starting agents...                           
    INFO[0008] Starting helpers...                          
    INFO[0008] Starting Node 'k3d-belajar-k3s-serverlb'     
    INFO[0015] Injecting '172.17.0.1 host.k3d.internal' into /etc/hosts of all nodes... 
    INFO[0015] Injecting records for host.k3d.internal and for 2 network members into CoreDNS configmap... 
    INFO[0016] Cluster 'belajar-k3s' created successfully!  
    INFO[0016] You can now use it like this:                
    kubectl cluster-info
    ```

2. Menghapus k3s cluster

    ```
    k3d cluster delete belajar-k3s
    ```


# Referensi #

* [Instalasi K3S](https://rancher.com/docs/k3s/latest/en/quick-start/) 